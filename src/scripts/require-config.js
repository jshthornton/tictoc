(function(win) {
	'use strict';

	var config = {
			baseUrl: chrome.extension.getURL('/scripts'),

			paths: {
				'jquery': 'lib/jquery',
				'Class': 'lib/Class',
				'text': 'lib/text',
				'underscore': 'lib/lodash'
			},

			shim: {
				Class: {
					exports: 'Class'
				}
			}
		};

	if('require' in win) {
		require.config(config);
	} else {
		win.require = config;
	}

	config = null;
}(window));