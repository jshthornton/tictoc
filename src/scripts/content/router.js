define([
	'./view/Login',
	'./view/Month',
	'./view/Day'
], function(LoginView, MonthView, DayView) {
	return {
		init: function() {
			var pathname = location.pathname,
				regex,
				fn,
				match;
			
			if(location.host === 'vpn.readingroom.com') {
				if(pathname.indexOf('timesheet.int.rroom.net') === -1) {
					return;
				}
				pathname = pathname.replace(',DanaInfo=timesheet.int.rroom.net+', '');
			}

			for(var key in this.routes) {
				if(this.routes.hasOwnProperty(key)) {
					regex = new RegExp(key);
					match = regex.exec(pathname);
					if(match !== null) {
						fn = this.routes[key];
						if(typeof this[fn] === 'function') {
							this[fn].apply(this, _.rest(match));
							break;
						}
					}
				}
			}

		},

		routes: {
			'/home': 'login',
			'^/(.*)/calendar(/([0-9]{4})/([0-9]{2}))?$': 'month',
			'/(.*)/calendar/([0-9]{4})/([0-9]{2})/([0-9]{2})/(.*)': 'editDay',
			'/(.*)/calendar/([0-9]{4})/([0-9]{2})/([0-9]{2})': 'day',
			'/(.*)/today': 'today'
		},

		login: function() {
			var loginView = new LoginView();
		},

		month: function() {
			var monthView = new MonthView();
		},

		editDay: function() {},

		day: function(username, year, month, day) {
			year = year >> 0;
			month = (month >> 0) - 1;
			day = (day >> 0);
			var dayView = new DayView(new Date(year, month, day), username);
		},

		today: function(username) {
			var dayView,
				date = new Date();
			date.setHours(0,0,0,0);
			dayView = new DayView(date, username);
		}
	};
});