require([
	'jquery',
	'content/router'
], function($, router) {
	$(function() {
		router.init();
	});
});