define([
	'Class'
], function() {
	var V = Class.extend({
		init: function() {
			if(!V.prototype.$pageForm) {
				V.prototype.$pageForm = $('#aspnetForm');
			}
			this.$pageForm.bind('submit.tictoc', $.proxy(this.onSubmit, this));
		},

		onSubmit: function() {}
	});

	return V;
});