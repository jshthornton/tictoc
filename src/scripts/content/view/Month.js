define([
	'jquery',
	'./_AppView',
	'underscore'
], function($, _AppView, _) {
	return _AppView.extend({
		init: function() {
			this._super();

			$.getJSON('http://ipinfo.io/json', {}, _.bind(function(response) {
				this.render(response.country);
			}, this));
		},

		render: function(country) {
			var MIN_HOURS,
				$days = $('table.month tbody td:not(.weekend):not(.previous)');

			switch(country) {
				case 'SG':
					MIN_HOURS = 8;
					break;
				default:
					MIN_HOURS = 7.5;
					break;
			}

			function getHours($node) {
				return parseFloat($node.text().replace(' hrs', ''));
			}
			
			$days.each(function(index, node) {
				var $node = $(node),
					hours = getHours($('.day a', $node));

				if(hours < MIN_HOURS && hours !== 0) {
					$node.addClass('TT_incomplete_day');
				}
			});

			$days.each(function(index, node) {
				var $node = $(node),
					hours = getHours($('.day a', $node));

				if(hours >= MIN_HOURS) {
					$node.addClass('TT_complete_day');
				}

				if($node.hasClass('today')) {
					return false;
				}
			});
		}
	});
});