define([
	'jquery',
	'./_AppView',
	'underscore',
	'text!./template/Day-Dates.jst'
], function($, _AppView, _, dateTemplate) {
	return _AppView.extend({
		$submit: null,

		init: function(date, username) {
			this._super();
			this.date = date;
			this.username = username;

			var $container = this.$container = $('#ctl00_cphBody_upType');
 
			// create an observer instance
			var observer = new WebKitMutationObserver($.proxy(this.onMutation, this));
			 
			observer.observe($container[0], {
				childList: true,
				subtree: true
			});

			this.render(date);
		},

		render: function() {
			var cmpl = _.template(dateTemplate),
				tmpl = cmpl({}),
				$date = $(tmpl);

			this.$startDate = $('#TT_start-date', $date);
			this.$endDate = $('#TT_end-date', $date);

			var dateString = this.date.getFullYear() + '-'
	             + ('0' + (this.date.getMonth()+1)).slice(-2) + '-'
	             + ('0' + this.date.getDate()).slice(-2);
	             

			this.$startDate[0].value = dateString;
			this.$endDate[0].value = dateString;

			this.$container.before($date);
		},

		//Events
		onSubmitClick: function(e) {
			var startDate = this.$startDate[0].valueAsDate,
				endDate = this.$endDate[0].valueAsDate,
				isProxy = (location.host === 'vpn.readingroom.com');

			if(endDate < startDate) {
				alert('End date can not be before start date');
				return false;
			}

			var data = {
				'ctl00$ScriptManager': 'ctl00$cphBody$upType|ctl00$cphBody$btnSubmit',
				'ctl00$cphBody$ddType': $('#ctl00_cphBody_ddType', this.$container).val(),
				'ctl00$cphBody$ddHours': $('#ctl00_cphBody_ddHours', this.$container).val(),
				'ctl00$cphBody$txtNotes': $('#ctl00_cphBody_txtNotes', this.$container).val(),
				'ctl00$cphBody$btnSubmit': 'Submit',
				'__LASTFOCUS': '',
				'__EVENTTARGET': '',
				'__EVENTARGUMENT': '',
				'__ASYNCPOST': true,
				'__VIEWSTATE': $('#__VIEWSTATE').val(),
				'__EVENTVALIDATION': $('#__EVENTVALIDATION').val()
			};

			if(data['ctl00$cphBody$ddType'].length === 0) {
				alert('Please select timesheet type');
				return false;
			}

			// Type client-work
			if(data['ctl00$cphBody$ddType'] === '1') {
				data['ctl00$cphBody$ddClient'] = $('#ctl00_cphBody_ddClient', this.$container).val();

				if(data['ctl00$cphBody$ddClient'].length === 0) {
					alert('Please select client');
					return false;
				}
			}

			// Type new biz
			if(data['ctl00$cphBody$ddType'] === '2') {
				data['ctl00$cphBody$ddClientNB'] = $('#ctl00_cphBody_ddClientNB', this.$container).val();

				if(data['ctl00$cphBody$ddClientNB'].length === 0) {
					alert('Please select client');
					return false;
				}
			}

			if(data['ctl00$cphBody$ddType'] === '1' || data['ctl00$cphBody$ddType'] === '2') {
				data['ctl00$cphBody$ddProject'] = $('#ctl00_cphBody_ddProject', this.$container).val();

				if(data['ctl00$cphBody$ddProject'].length === 0) {
					alert('Please select project');
					return false;
				}

				var $order = $('#ctl00_cphBody_ddOrder', this.$container);
				if($order.length) {
					data['ctl00$cphBody$ddOrder'] = $order.val();
					
					if(data['ctl00$cphBody$ddOrder'].length === 0) {
						alert('Please select work package');
						return false;
					}
				}

			}

			// Type non-client
			if(data['ctl00$cphBody$ddType'] === '3') {
				data['ctl00$cphBody$ddActivity'] = $('#ctl00_cphBody_ddActivity', this.$container).val();

				var $activityDetails = $('#ctl00_cphBody_ddActivityDetails', this.$container);
				if($activityDetails.length) {
					data['ctl00$cphBody$ddActivityDetails'] = $activityDetails.val();

					if(data['ctl00$cphBody$ddActivityDetails'].length === 0) {
						alert('Please select activity details');
						return false;
					}
				}
			}
			
			if(data['ctl00$cphBody$ddHours'].length === 0) {
				alert('Please select hours');
				return false;
			}

			
			$(document.body).addClass('TT_pending');

			setTimeout(_.bind(function() {
				var responses = [],
					url,
					resp,
					day;

				for (var d = new Date(startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
					day = d.getDay();
					console.log(day);
					if(day === 0 || day === 6) {
						continue;
					}

					if(isProxy) {
						url = '/' + this.username + '/calendar/' + d.getFullYear() + '/' + (d.getMonth() + 1) + '/,DanaInfo=timesheet.int.rroom.net+' + d.getDate();
					} else {
						url = '/' + this.username + '/calendar/' + d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate();
					}

					resp = $.ajax({
						url: url,
						type: 'POST',
						data: data
					});
					responses.push(resp);
				}

				$.when.apply($, responses).done(function() {
					window.location.reload();
				});
			}, this), 0);


			return false;
		},

		onMutation: function(mutations) {
			var _this = this;
			mutations.forEach(function(mutation) {
				for (var i = 0; i < mutation.addedNodes.length; i++) {
					var addedNode = mutation.addedNodes[i],
						$submit;

					$submit = $('#ctl00_cphBody_btnSubmit', addedNode);
					if($submit.length) {
						_this.$submit = $submit;
						_this.$submit.unbind('click').bind('click', $.proxy(_this.onSubmitClick, _this));
					}
				}
			}); 
		}
	});
});