define([
	'./_AppView'
], function(_AppView) {
	return _AppView.extend({
		init: function() {
			this._super();

			this.$loginSelect = $('#ctl00_cphBody_ddStaff');
			var userId = localStorage.getItem('userId');

			if(userId !== undefined) {
				this.$loginSelect.val(userId);
			}
		},

		//Events
		onSubmit: function(e) {
			var userId = this.$loginSelect[0].selectedOptions[0].value;
			
			localStorage.setItem('userId', userId);
		}
	});
});